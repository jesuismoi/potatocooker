package com.jesuismoi.PotatoCooker;

import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Players;

public class MoveToRange extends Task {
    @Override
    public boolean activate() {
        //Has potatoes && not near range, or can't reach range

        Position rangePosition = PotatoCooker.getRangePosition();
        if (rangePosition != null) {
            return Inventory.contains("Potato")
                    && (rangePosition.distance() > 15 || !rangePosition.isPositionInteractable());
        }

        return false;
    }

    @Override
    public boolean execute() {
        Position rangeLoc = PotatoCooker.getRangePosition();
        Player p = Players.getLocal();

        if (rangeLoc != null && p != null) {

            Time.sleepUntil(()-> {
                Movement.walkTo(rangeLoc);
                return p.distance(rangeLoc) <= 10 && rangeLoc.isPositionInteractable();
            }, Random.nextInt(1500, 4000), Random.nextInt(7000, 20000));

            return p.distance(rangeLoc) <= 10;
        }
        
        return false;
    }

    @Override
    public String toString() {
        return "Walking to range";
    }
}
