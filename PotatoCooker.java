package com.jesuismoi.PotatoCooker;

import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.Script;
import org.rspeer.script.ScriptMeta;
import org.rspeer.ui.Log;

import java.util.ArrayList;

@ScriptMeta(developer="jesuismoi", name="PotatoCooker", desc="Cooks potatoes. Start near any range")
public class PotatoCooker extends Script {

    private ArrayList<Task> tasks;
    private static Position rangePosition;
    private static int stop = 0;
    @Override
    public int loop() {
        //Check if program should be stopped, not sure if this is the proper way to do it
        if (stop == -1) {
            setStop();
        }

        //Initialize range coordinate if not already done
        if (rangePosition == null) {
            SceneObject range = SceneObjects.getNearest("Range");
            if (range != null) {
                rangePosition = range.getPosition();
            }
        }

        //Check if tasks need to be completed
        for (Task t : tasks) {
            if (t.activate()) {
                t.execute();
                Log.info(t);
            }
        }


        return Random.nextInt(400,600);
    }

    @Override
    public void onStart() {
        tasks = new ArrayList<>();
        
        tasks.add(new Cook());
        tasks.add(new MoveToBank());
        tasks.add(new UseBank());
        tasks.add(new MoveToRange());

        //TODO: Ask user for input on range position
    }

    public static Position getRangePosition() {
        return rangePosition;
    }

    public static void setStop() {
        stop = -1;
    }
}
