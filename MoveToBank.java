package com.jesuismoi.PotatoCooker;

import org.rspeer.runetek.api.commons.BankLocation;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Players;

public class MoveToBank extends Task {
    @Override
    public boolean activate() {
        BankLocation nearest = BankLocation.getNearest();
        if (nearest != null) {
            Position bankPos = nearest.getPosition();
            if (bankPos != null) {
                // Return true if far from bank, or not able to interact with bank, and no food left to cook
                return (bankPos.distance() > 15 || !bankPos.isPositionInteractable())
                        && !Inventory.contains("Potato");
            }
        }
        return false;
    }

    @Override
    public boolean execute() {
        BankLocation nearest = BankLocation.getNearest();
        if (nearest != null) {
            Position bankPos = nearest.getPosition();
            if (bankPos != null) {

                // Walk towards nearest bank until distance <10 and able to use bank
                Time.sleepUntil(()-> {
                    Movement.walkTo(nearest.getPosition());
                    return Players.getLocal().distance(bankPos) <= 10 && bankPos.isPositionInteractable();
                }, Random.polar(1500, 4000), Random.nextInt(7000, 20000));

                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "Pathing";
    }
}
