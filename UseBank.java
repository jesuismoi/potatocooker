package com.jesuismoi.PotatoCooker;

import com.jesuismoi.PotatoCooker.PotatoCooker;
import com.jesuismoi.PotatoCooker.Task;
import org.rspeer.runetek.api.commons.BankLocation;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.position.Position;

public class UseBank extends Task {
    @Override
    public boolean activate() {
        BankLocation nearest = BankLocation.getNearest();

        if (nearest != null) {
            Position bankPos = nearest.getPosition();
            if (bankPos != null && !Inventory.contains("Potato")) {
                //Wait a bit for the inventory check since withdrawing is not instant
                if (!Time.sleepUntil(()->Inventory.getCount("Potato") == 28, Random.high(70, 140), Random.nextInt(200, 300))) {
                    //Use bank if near bank and no more potatos to cook and can interact with bank
                    return bankPos.distance() <= 15
                            && !Inventory.contains("Potato")
                            && bankPos.isPositionInteractable();
                }
            }
        }
        return false;
    }

    @Override
    public boolean execute() {
        if (!Bank.isOpen()) {
            Bank.open();
            Time.sleepUntil(()->Bank.isOpen(), Random.polar(300, 400), Random.high(10000, 15000));
        }

        if (Bank.isOpen() && Bank.depositInventory()){
            Time.sleepUntil(()->Inventory.isEmpty(), Random.polar(400, 600), Random.high(1980, 2019));

            if (Bank.getCount("Potato") == 0) {
                PotatoCooker.setStop();
            }

            if(Bank.withdraw("Potato", 28)) {
                Time.sleepUntil(()->Inventory.isFull(), Random.polar(500, 743), 2000);
                return Inventory.getCount("Potato") == 28;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "Using Bank";
    }
}
