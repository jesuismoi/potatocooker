package com.jesuismoi.PotatoCooker;

import org.rspeer.runetek.adapter.component.InterfaceComponent;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;

public class Cook extends Task {
    @Override
    public boolean activate() {
        SceneObject range = SceneObjects.getNearest("Range");
        Player p = Players.getLocal();

        if (range != null && p != null && !p.isAnimating()) {
            //Add a delay to ensure that the player is really not performing an animation
            if(!Time.sleepUntil(()->p.isAnimating(), Random.high(70, 150), Random.high(432, 532))) {
                //Return true if near a range, have food, not performing an animation, and able to interact with range
                return p.distance(range) <= 10
                        && Inventory.contains("Potato")
                        && !p.isAnimating()
                        && range.isPositionInteractable();
            }
        }
        return false;
    }

    @Override
    public boolean execute() {
        SceneObject range = SceneObjects.getNearest("Range");
        Player p = Players.getLocal();

        if (Bank.isOpen()) {
            Bank.close();
        }

        /* Null check then interact with range. Wait until the make all option is visible then select it and wait until
        * player is interacting with range */
        if (range != null && p != null && range.interact("Cook")) {
            return Time.sleepUntil(()->{
                InterfaceComponent makeAllComponent = Interfaces.getComponent(270, 14);
                if (makeAllComponent != null && makeAllComponent.isVisible()) {
                    if (makeAllComponent.interact("Cook")) {
                        return Time.sleepUntil(()->p.isAnimating(), Random.polar(1000, 5000), Random.polar(3800, 6100));
                    }
                }
                return false;
            }, Random.polar(400, 600), Random.nextInt(15000, 20000));
        }
        return false;
    }

    @Override
    public String toString() {
        return "Cooking Potatoes";
    }
}
