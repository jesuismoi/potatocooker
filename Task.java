package com.jesuismoi.PotatoCooker;

public abstract class Task {
    public abstract boolean activate();
    public abstract boolean execute();
    public abstract String toString();
}
